'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var concatCss = require('gulp-concat-css');

gulp.task('sass', function () {
    return gulp.src('./css/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false,
            options: {
                browsers: ['ie >= 11']
            }
        }))
        .pipe(gulp.dest('./css'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('browser-sync', function () {
    var files = [
        './style.css',
        './*.php'
    ];
    browserSync(files, {
        // server: {
        //     baseDir: './'
        // },
        proxy: "http://adiam/",
        notify: false
    });
});

gulp.task('watch', ['browser-sync', 'sass'], function () {
    gulp.watch('./css/scss/**/*.scss', ['sass']);
});

gulp.task('scripts', function() {
    return gulp.src(['./js/main.js'])
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./js/'));
});

gulp.task('styles', function () {
    return gulp.src(['./css/normalize.css', './css/style.css'])
        .pipe(concatCss("all.css"))
        .pipe(gulp.dest('./css/'));
});


