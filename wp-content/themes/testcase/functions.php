<?php
/**
 * STYLES and SCRIPTS
 **/
 function load_style(){
     wp_register_style('normalize', get_template_directory_uri() . '/css/normalize.css');
     wp_register_style('style', get_template_directory_uri() . '/css/style.css');
     wp_enqueue_style('normalize');
     wp_enqueue_style('style');
 }
 function load_script(){
     wp_register_script('main', get_template_directory_uri() . '/js/javascript.js', array(), false, true);
     wp_enqueue_script('main');
 }


/**
 * ACTIONS
 **/
 add_action('wp_enqueue_scripts', 'load_script');
 add_action('wp_enqueue_scripts', 'load_style');


/**
 * MENU
 **/
register_nav_menus( array(
    'main_menu'=> 'Main menu'
) );

/*menu active class*/
//add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
//
//function special_nav_class ($classes) {
//    if (in_array('current-menu-item', $classes) ){
//        $classes[] = 'active';
//    }
//    return $classes;
//}

