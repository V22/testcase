<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'testcase');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


//define( 'WPLANG', 'ua_UA' );
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'BW?1BrUC]DM?E:j|G*<]4r8={f{,%?%iA}+ULCdt^Rt)bNg)nBi0veFn:FFBarnb');
define('SECURE_AUTH_KEY',  'Cxivpq*oad9IR>)+ l*zWK9N6.ssmO4:Ap6h{3!{/22I7QLoZ6B>,hBPJ~Zs/.CZ');
define('LOGGED_IN_KEY',    'sr;hB)zGK|i,vi4EPZdzO>;lXT+WXC$DQXw1DA;8DP<BR]iU+1|jB<]#D;.;EQgw');
define('NONCE_KEY',        '8-v7A$:AVUhv_ufBH%0.k*?wO^{H*e~5/[0>yv/KXg--rq*DJoyhmDMzo?EX+x7p');
define('AUTH_SALT',        'q|L+q{>Tl|<W_w}OOL{ZwqyQf-Om0`ClsWe5,VG3iFyaqO%sUfM#V UM/m5w%uy/');
define('SECURE_AUTH_SALT', ',V[`~RSS1:$v(J0x,^|(G`;f*wur{t{cYL|-/8R6}_|/kjs6ap&(-TpoQLSEK @A');
define('LOGGED_IN_SALT',   '<iA@#+rJ9:_5%hoTrj47cxb-}_x-*E[FH-l+Ei#>A~Bt]yy/Zb.9-+=`|ILjTf.8');
define('NONCE_SALT',       '7MMMg5|<P0oA5-AD:t2EJd&]ox[8|CN-dJ)g]~K}0[u-XAR/3j!v19<LR{8DOGj@');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'v22_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
